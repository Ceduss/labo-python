from randomword import *
     

word = RandomWord()
letters_found = []
letters_not_in_word = []
nb_errors = 0
win = False
body_pattern = ["O", "/", "|", "\\", "/", "\\"]
body_inprogress = [" ", " ", " ", " ", " ", " "]

 
while not win:

    win = True 
    print(" +---+")
    print(" |   |")
    print(" |   {}".format(body_inprogress[0]))
    print(" |  {}{}{}".format(body_inprogress[1], body_inprogress[2], body_inprogress[3]))
    print(" |  {} {}".format(body_inprogress[4], body_inprogress[5]))
    print("_|_")
    print("| |")

   

    for letter in word:
        if letter in letters_found:
            print(letter, end=" ")
        else:
            win = False 
            print("_", end=" ")
    
    print("\nLettres utilisées: ", end="")
    for letter in letters_not_in_word:
        print(letter, end=" | ")

    if nb_errors > 5:
        print("\nPERDU eheh")
        print("\n Le mot étais: ", word)
        break

    if win:
        print("\nBien joué !")
        break

    letter = input("\nEntrez un lettre: ").upper()

    if letter in word:
        letters_found.append(letter)
    else:
        letters_not_in_word.append(letter)

    if letter not in word:
        body_inprogress[nb_errors] = body_pattern[nb_errors]
        nb_errors += 1