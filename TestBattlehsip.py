n = 11
m = 11

grid = [[x * y for x in range(1,n)] for y in range(1,m)]

print('   ', end='')
print('   ', end='')
print("''.join([f'{j:5}' for j in range(1,n)])")
print('   ', end='')
print(''.join([f'{"_":>5}' for _ in range(1,n)]))

for i in range(n-1):
        print(f'{i+1:2}|', end=' ')
        print(' '.join(f'{x:4}' for x in grid[i]))