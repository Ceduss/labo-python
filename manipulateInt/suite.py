

def suite():
    m = input("Choisissez un entier entre 1 et 106: ")
    n = int(m)


    if 0<n and n<107:
        res = m

        while n != 1:
            if n % 2 == 0:
                n = n//2
                res += "->" + str(n)
        
            else:
                n = n*3+1
                res += "->" + str(n)
        print(res)
    else:
        print("Veuillez entrer un entier entre 1 et 106.")
        suite()
    
suite()