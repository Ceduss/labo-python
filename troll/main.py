import pygame 

pygame.init()
screen = pygame.display.set_mode((400, 400))
running = True
image = pygame.image.load("troll/troll.png").convert()

x = 0
y = 0   
clock = pygame.time.Clock()

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        
       

    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_LEFT] | pressed[pygame.K_q]:
        x -= 1
    if pressed[pygame.K_RIGHT] | pressed[pygame.K_d]:
        x += 1
    if pressed[pygame.K_UP] | pressed[pygame.K_z]:
        y -= 1
    if pressed[pygame.K_DOWN] | pressed[pygame.K_s]:
        y += 1
    if pressed[pygame.K_ESCAPE]:
        pygame.quit()

    screen.fill((0, 0, 0))    
    screen.blit(image, (x, y))  
    pygame.display.flip()
    clock.tick(500)

pygame.quit()